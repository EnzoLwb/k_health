<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

//问卷测试的选项
class qOption extends Model
{
    protected $table = 'health_q_options';
    protected $primaryKey = 'id';
}
