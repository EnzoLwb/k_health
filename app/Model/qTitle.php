<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

//问卷测试的题目
class qTitle extends Model
{
    protected $table = 'health_q_title';
    protected $primaryKey = 'id';
}
