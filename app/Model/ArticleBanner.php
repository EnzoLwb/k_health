<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ArticleBanner extends Model
{
    protected $table = 'health_banner';
    protected $primaryKey = 'id';
}
