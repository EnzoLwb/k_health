<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

//图组的数据来源 channel_id是栏目的id
class GroupPic extends Model
{
    protected $table = 'group_pic';
    protected $primaryKey = 'id';
}
