<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

//问卷测试的结论分数
class qResult extends Model
{
    protected $table = 'health_q_results';
    protected $primaryKey = 'id';
}
