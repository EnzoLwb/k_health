<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

//问卷测试的主题
class Questionnaire extends Model
{
    protected $table = 'health_questionnaire';
    protected $primaryKey = 'id';
}
