<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

//channel_id是栏目号，sub_channel_id是子栏目号，category是孙栏目号
class Channel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'octree_matrix.channel';
}
