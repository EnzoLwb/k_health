<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HotSearch extends Model
{
    protected $table = 'health_hot_search';
    protected $primaryKey = 'id';
}
