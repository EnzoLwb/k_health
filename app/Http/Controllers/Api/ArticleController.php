<?php

namespace App\Http\Controllers\Api;

use App\Model\Article;
use App\Model\ArticleBanner;
use App\Model\Channel;
use App\Model\HotSearch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    //首页/膳食/医药/科普  列表
    public function index()
    {
        $category_id = \request('category_id',1713);
        $page = \request('page',1);
        $limit = \request('page_size',10);
        $offset = ($page - 1) * $limit;
        $search = \request('search');//搜索关键词
        //查询二级分类
        $channel_ids = Channel::where('parent_id',$category_id)->pluck('id');
        $data = Article::leftjoin('octree_matrix.channel','channel.id','health_article.channel_id')
            ->whereIn('octree_matrix.channel.parent_id',$channel_ids)
            ->when($search, function ($query) use ($search) {
                return $query->where('title','like', '%'.$search.'%');
                        /*->orWhere(function ($query) use ($search){
                        $query->where('content', 'like', '%'.$search.'%');
                    });*///文章内容也模糊搜索
            })
            ->orderBy('show_priority','desc')
            ->offset($offset)->limit($limit)
            ->select('author','title','name','health_article.id','cover')
            ->get();
        return $this->json(0,'',$data);
    }

    //获取栏目列表(首页1713 膳食1722 医药1721 科普1750) 除了健康自测
    public function category_channel()
    {
        $category_id = \request('category_id',1713);
        $data = Channel::where('parent_id',$category_id)->select('channel_id','name')->get();
        return $this->json(0,'',$data);
    }

    //banner列表 (首页1713 膳食1722 医药1721 科普1750)
    public function banner()
    {
        $category_id = \request('category_id',1713);
        $data = ArticleBanner::where('category_id',$category_id)
            ->where('status',true)
            ->orderBy('weight','desc')
            ->select('title','article_id','cover')
            ->get();
        return $this->json(0,'',$data);
    }

    //频道列表 二级的
    public function channel_list()
    {
        $channel_id = \request('channel_id',1715);//养生一组
        $page = \request('page',1);
        $limit = \request('page_size',10);
        $offset = ($page - 1) * $limit;
        $data = Article::leftjoin('octree_matrix.channel','octree_matrix.channel.id','health_article.channel_id')
            ->where('octree_matrix.channel.parent_id',$channel_id)//下级目录 #xxxx
            ->orWhere('octree_matrix.channel.id', $channel_id) //同级目录 频道
            ->orderBy('show_priority','desc')
            ->offset($offset)->limit($limit)
            ->select('author','title','name','health_article.id','cover')
            ->get();
        return $this->json(0,'',$data);
    }

    //搜索页(热门搜索+热门推荐)
    public function search()
    {
        //熱門搜索
        $data['search'] = HotSearch::where('status',true)->orderBy('weight','desc')->pluck('name');
        //热门推荐
        $data['recommend'] = $this->recommend();
        return $this->json(0,'',$data);
    }

    //图文详情
    public function article_detail()
    {
        $article_id = \request('article_id');
        $data['article'] = Article::find($article_id);
        if (!$data['article']) return $this->json(1,'无此文章',[]);
        //热门 推荐
        $data['recommend'] = $this->recommend();
        //查询二级类目 名称和id
        $data['other_article']['category_id'] = Channel::find($data['article']->channel_id)->parent_id;//用于查看更多的频道列表
        $data['other_article']['category_name'] = Channel::find($data['other_article']['category_id'])->name;
        //同类的其他文章
        $data['other_article'] = $this->other_article($data['other_article']['category_id'],3,$data['article']->id);
        return $this->json(0,'',$data);
    }

}
