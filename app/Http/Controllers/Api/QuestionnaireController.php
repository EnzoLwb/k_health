<?php

namespace App\Http\Controllers\Api;

use App\Model\qOption;
use App\Model\qResult;
use App\Model\qTitle;
use App\Model\Questionnaire;
use App\Http\Controllers\Controller;

class QuestionnaireController extends Controller
{
    //问卷列表
    public function index()
    {
        $data = Questionnaire::where('status',true)->orderBy('weight','desc')->select('title','id as object_id','category_name','cover')->get();
        return $this->json(0,'',$data);
    }

    //测试详情 包括推荐测试 热门推荐
    public function detail()
    {
        $object = \request('object_id');
        if (!$object) return $this->json(1,'缺少参数~',[]);
        //题目
        $title = qTitle::where('object_id',$object)
            ->orderBy('order_id','desc')
            ->pluck('content','id');
        //选项
        $options = qOption::where('object_id',$object)
            ->select('subject_id','id as option_id','option')->get();//subject_id 对应的题目id
        foreach ($options as $v){
            if (isset($title[$v->subject_id])){
                $title[$v->subject_id]['options'][] =  ['option_id' => $v->option_id,'option_content' => $v->option];
            }
        }
        $data['subject'] = $title;

        //推荐测试
        $data['recommend_exam'] = Questionnaire::where('status',true)->orderBy('weight','desc')->select('title','id as object_id','category_name','cover')->limit(4)->get();

        //热门文章推荐
        $data['recommend'] = $this->recommend();
        return $this->json(0,'',$data);

    }

    //提交结果 返回结果
    public function result()
    {
        $object_id = request('object_id');
        $option_ids = request('option_ids');//[1,3,5] 选择的选项数组
        $score = qOption::whereIn('id',$option_ids)->sum('score');
        $average = round($score / count($score) ,2);
        $result = qResult::where('object_id',$object_id)
            ->where('score_min','>',$average)
            ->where('score_max','<',$average)
            ->select('result','title')->get();
        return $this->json(0,'',$result);
    }

}
