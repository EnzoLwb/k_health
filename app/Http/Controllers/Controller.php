<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Model\Article;
use App\Model\Channel;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //0 表示成功  其他看message返回
    public function json($code,$message='',$list=[])
    {
        return response()->json(compact('code','message','list'));
    }

    //热门推荐(随机分类)
    public function recommend()
    {
        //随机选择一个分类 或者是后台控制
        $all_sec_category = Channel::whereIn('parent_id',[1713,1722,1721,1750])->pluck('id')->toArray();  //所有二级分类
        $category_id  = array_random($all_sec_category);
        $data = $this->other_article($category_id);
        return $data;
    }

    //二级分类下的的其他文章 $category_id是二级分类id
    public function other_article($category_id,$limit = 3,$not_article_id = 0)
    {
        $data = Article::leftjoin('octree_matrix.channel','octree_matrix.channel.id','health_article.channel_id')
            ->Where('octree_matrix.channel.parent_id', $category_id)
            ->Where('health_article.id','!=', $not_article_id)
            ->orderBy('show_priority','desc')
            ->limit($limit)
            ->select('author','title','name','health_article.id','cover','octree_matrix.channel.parent_id')
            ->get();
        return $data ;
    }
}
