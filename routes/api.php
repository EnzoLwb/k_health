<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace' => 'Api',
], function () {
    Route::post('home', 'ArticleController@index');//栏目文章和视频列表(首页 膳食 医药 科普)
    Route::post('banner', 'ArticleController@banner');//每个栏目的banner
    Route::post('channel_list', 'ArticleController@channel_list');//频道(二级栏目)文章和视频列表
    Route::post('category_channel', 'ArticleController@category_channel');//获取栏目下的频道信息(首页 膳食 医药 科普)
    Route::post('article_detail', 'ArticleController@article_detail');//图文详情
    Route::post('search', 'ArticleController@search');//搜索页(热门搜索+热门推荐)

    Route::group([
        'prefix' => 'question',
    ], function () {
        Route::post('/', 'QuestionnaireController@index');
        Route::post('/detail', 'QuestionnaireController@detail');//测试详情 包括推荐测试 热门推荐
        Route::post('/result', 'QuestionnaireController@result');//提交结果 返回结果


    });


});

